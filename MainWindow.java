import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import jdk.nashorn.internal.runtime.JSType;

/**
 *
 * @author Dinz
 */
public class MainWindow extends javax.swing.JFrame {

    private final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private final DecimalFormat dc = new DecimalFormat("###,###");
    private DefaultTableModel tableModel;
    private ArrayList<Kamar> kamar;
    private int indexKode = 1000;
    private long subTotal = 0, lama;
    private String nota;

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();
        initData();
        this.setLocationRelativeTo(null);
        reportDialog.setLocationRelativeTo(null);
        reportDialog.pack();
    }
    
    private void showDialog(){
        reportDialog.setVisible(true);
    }

    private void initData() {
        resetFormPelanggan();
        resetFormKamar();
        kamar = new ArrayList();

        df.setLenient(false);

        indexKode++;

        noTransField.setText("S" + indexKode);
        tglTransField.setText(df.format(new Date()));
        inField.setText(df.format(new Date()));
        subTotalField.setValue(null);
        tambahanField.setValue(null);
        totalField.setValue(null);
        buildTable();
    }

    public final void buildTable() {
        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Key");
        tableModel.addColumn("#");
        tableModel.addColumn("No. Kamar");
        tableModel.addColumn("Tipe Kamar");
        tableModel.addColumn("Jenis Kamar");
        tableModel.addColumn("Tarif Kamar");

        for (int i = 0; i < kamar.size(); i++) {
            tableModel.addRow(new Object[]{
                i, // Key -> Untuk keperluan ubah dan hapus data
                i + 1, // Nomor urut di tabel
                kamar.get(i).getNomorKamar(),
                kamar.get(i).getTipeKamar(),
                kamar.get(i).getJenisKamar(),
                dc.format(kamar.get(i).getTarif()),});
        }
        tabelKamar.setModel(tableModel);

        TableColumnModel cm = tabelKamar.getColumnModel();
        cm.removeColumn(cm.getColumn(0));
        tabelKamar.getColumnModel().getColumn(0).setMaxWidth(40);
    }

    private void setKamar(String val) {
        String tipeKamar = val.substring(0, 1);

        if (tipeKamar.equals("1")) {
            tipeKamarField.setText("1");
            jenisKamarField.setText("Double Bed");
            tarifField.setValue(700000);
        } else if (tipeKamar.equals("2")) {
            tipeKamarField.setText("1");
            jenisKamarField.setText("Single Bed");
            tarifField.setValue(500000);
        } else {
            alertWarning("Nomor kamar salah! Silahkan periksa!");
        }
    }

    private void tambahKamar() {

        kamar.add(new Kamar(
                Integer.valueOf(noKamarField.getText()),
                Integer.valueOf(tipeKamarField.getText()),
                jenisKamarField.getText(),
                Long.valueOf(String.valueOf(tarifField.getValue()))));

        buildTable();
        resetFormKamar();
        setSubTotal();
        noKamarField.requestFocus();
    }

    private long getLama() {
        Calendar cin = Calendar.getInstance();
        Calendar cout = Calendar.getInstance();

        try {
            cin.setTime(df.parse(inField.getText()));
            cout.setTime(df.parse(outField.getText()));
        } catch (ParseException ex) {
            return -1;
        }
        return ChronoUnit.DAYS.between(cin.toInstant(), cout.toInstant()) + 1;
    }

    private void hapusKamar(int index) {
        kamar.remove(index);
        buildTable();
        setSubTotal();
    }

    private void resetFormKamar() {
        noKamarField.setValue(null);
        tipeKamarField.setText("");
        jenisKamarField.setText("");
        tarifField.setValue(null);
    }

    private void resetFormPelanggan() {
        ktpField.setText("");
        hpField.setText("");
        pelangganField.setText("");
        outField.setValue(null);
        depositField.setValue(null);
    }

    private void simpanTransaksi() {
        if (!isFormValid()) {
            return;
        }

        String ktp = ktpField.getText(),
                hp = hpField.getText(),
                pelanggan = pelangganField.getText(),
                in = inField.getText(),
                out = outField.getText(),
                deposit = depositField.getValue().toString();

        if (kamar.isEmpty()) {
            alertWarning("Silahkan isi data pesan kamar!");
            return;
        }

        nota = ""
                + "---------------------------------------------------------\n"
                + "------------------- HOTEL NYAMAN SEKALI -----------------\n"
                + "---------------------------------------------------------\n"
                + "No. Transaksi             : " + noTransField.getText() + "\n"
                + "Tgl. Transaksi            : " + tglTransField.getText() + "\n"
                + "---------------------------------------------------------\n"
                + "Pelanggan                 : " + pelanggan + "\n"
                + "Nomor KTP                 : " + ktp+"\n"
                + "Nomor HP                  : " + hp+"\n"
                + "---------------------------------------------------------\n"
                + "Tanggal Check In          : " + in + "\n"
                + "Tanggal Check Out         : " + out + "\n"
                + "Deposit                   : " + dc.format(Long.valueOf(deposit)) + "\n"
                + "---------------------------------------------------------\n"
                + "Kamar :\n";
        
        for(int i=0; i<kamar.size(); i++){
            nota += kamar.get(i).getNomorKamar()+" \t"
                    + kamar.get(i).getTipeKamar()+" \t"
                    + kamar.get(i).getJenisKamar()+" \t"
                    + kamar.get(i).getTarif()+" \n";
        }
        
        nota += "Biaya tambahan            : " + dc.format(Long.valueOf(tambahanField.getValue().toString())) + "\n";
        nota += "---------------------------------------------------------\n";
        nota += "Total                     : " + dc.format(Long.valueOf(totalField.getValue().toString())) + "\n";
        
        notaArea.setText(nota);
        initData();
        showDialog();
//        mainPane.setSelectedIndex(1);
    }

    private boolean isFormValid() {
        if (!JSType.isNumber(ktpField.getText())) {
            alertWarning("Format penulisan Nomor KTP salah! Silahkan periksa!");
            ktpField.requestFocus();
            return false;
        }

        if (!JSType.isNumber(hpField.getText())) {
            alertWarning("Format Nomor HP salah! Silahkan periksa!");
            hpField.requestFocus();
            return false;
        }

        if (pelangganField.getText().length() == 0) {
            alertWarning("Nama pelanggan tidak boleh kosong! Silahkan periksa!");
            pelangganField.requestFocus();
            return false;
        }

        if (!pelangganField.getText().matches("[a-zA-Z ]*")) {
            alertWarning("Nama pelanggan tidakboleh berisi angka! Silahkan periksa!");
            pelangganField.requestFocus();
            return false;
        }

        try {
            df.parse(inField.getText());
        } catch (Exception e) {
            alertWarning("Format tanggal check in salah!");
            inField.requestFocus();
            return false;
        }

        try {
            if (df.parse(inField.getText()).before(df.parse(df.format(new Date())))) {
                alertWarning("Tanggal check in tidak boleh kurang dari hari ini!");
                return false;
            }
        } catch (ParseException ex) {
            alertWarning("Format tanggal check in salah!");
        }

        try {
            df.parse(outField.getText());
        } catch (Exception e) {
            alertWarning("Format tanggal check out salah!");
            outField.requestFocus();
            return false;
        }

        try {
            if (df.parse(inField.getText()).after(df.parse(outField.getText()))) {
                alertWarning("Tanggal check out harus lebih dari tanggal check in!");
                return false;
            }
        } catch (ParseException ex) {
            alertWarning("Format tanggal check out salah!");
        }

        if (depositField.getText().trim().length() == 0 || 
                !JSType.isNumber(depositField.getValue().toString())) {
            alertWarning("Format deposit salah! Silahkan periksa!");
            depositField.requestFocus();
            return false;
        }
        return true;
    }

    private void setSubTotal() {
        if (getLama() > 0) {
            subTotalField.setValue(getLama() * getTotalHargaKamar());
        }else{
            subTotalField.setValue(getTotalHargaKamar());
        }
        
    }

    private void setTotalTagihan() {
        long t = 0, d = 0;

        try {
            t = Long.valueOf(tambahanField.getValue().toString());
        } catch (Exception e) {
            if (tambahanField.getText().trim().length() != 0) {
                alertWarning("Format tambahan salah!");
                return;
            }
        }

        try {
            d = Long.valueOf(depositField.getValue().toString());
        } catch (Exception e) {
            if (depositField.getText().trim().length() != 0) {
                alertWarning("Format deposit salah!");
                return;
            }
        }

        totalField.setValue((getLama() * getTotalHargaKamar() + t) - d);
    }

    private long getTotalHargaKamar() {
        if (kamar.isEmpty()) {
            return 0;
        }
        long total = 0;
        for (int i = 0; i < kamar.size(); i++) {
            total += kamar.get(i).getTarif();
        }
        return total;
    }

    private void alertWarning(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Peringatan", 2);
    }

    private void alertInfo(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Info", 1);
    }

    private void alert(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Info", 1);
    }
    
    private boolean confirm(String msg) {
        return JOptionPane.showConfirmDialog(this, msg, "Info", JOptionPane.OK_CANCEL_OPTION) 
                == JOptionPane.OK_OPTION;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        reportDialog = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        notaArea = new javax.swing.JTextArea();
        mainPane = new javax.swing.JTabbedPane();
        transPane = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        noTransField = new javax.swing.JTextField();
        tglTransField = new javax.swing.JTextField();
        ktpField = new javax.swing.JTextField();
        hpField = new javax.swing.JTextField();
        pelangganField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        inField = new javax.swing.JFormattedTextField();
        outField = new javax.swing.JFormattedTextField();
        depositField = new javax.swing.JFormattedTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        tipeKamarField = new javax.swing.JTextField();
        jenisKamarField = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        tambahKamarButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelKamar = new javax.swing.JTable();
        prosesButton = new javax.swing.JButton();
        previewButton = new javax.swing.JButton();
        noKamarField = new javax.swing.JFormattedTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        tambahanField = new javax.swing.JFormattedTextField();
        subTotalField = new javax.swing.JFormattedTextField();
        totalField = new javax.swing.JFormattedTextField();
        tarifField = new javax.swing.JFormattedTextField();
        previewPane = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        keluarButton = new javax.swing.JButton();

        reportDialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel17.setText("DATA TAGIHAN");

        notaArea.setColumns(20);
        notaArea.setFont(new java.awt.Font("Monospaced", 0, 15)); // NOI18N
        notaArea.setRows(5);
        jScrollPane2.setViewportView(notaArea);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addGap(0, 469, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout reportDialogLayout = new javax.swing.GroupLayout(reportDialog.getContentPane());
        reportDialog.getContentPane().setLayout(reportDialogLayout);
        reportDialogLayout.setHorizontalGroup(
            reportDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        reportDialogLayout.setVerticalGroup(
            reportDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel3.setText("No. Transaksi");

        noTransField.setEditable(false);
        noTransField.setBackground(new java.awt.Color(255, 204, 204));

        tglTransField.setEditable(false);
        tglTransField.setBackground(new java.awt.Color(255, 204, 204));

        jLabel10.setText("Deposit");

        jLabel9.setText("Tgl Check Out");

        jLabel8.setText("Tgl Check In");

        jLabel7.setText("Nama Pelanggan");

        jLabel6.setText("No. HP");

        jLabel5.setText("No. KTP");

        jLabel4.setText("Tgl Transaksi");

        try {
            inField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        inField.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                inFieldPropertyChange(evt);
            }
        });

        try {
            outField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        outField.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                outFieldPropertyChange(evt);
            }
        });

        depositField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        depositField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        depositField.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                depositFieldPropertyChange(evt);
            }
        });
        depositField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                depositFieldKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(noTransField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(tglTransField, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(ktpField, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(hpField, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(depositField)
                                    .addComponent(outField, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(pelangganField)
                                            .addComponent(inField, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE))
                                        .addGap(0, 0, Short.MAX_VALUE)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(noTransField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tglTransField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(ktpField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(hpField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(pelangganField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(inField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(outField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(depositField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jLabel13.setText("No. Kamar");

        jLabel14.setText("Tipe Kamar");

        tipeKamarField.setEditable(false);
        tipeKamarField.setBackground(new java.awt.Color(255, 204, 204));

        jenisKamarField.setEditable(false);
        jenisKamarField.setBackground(new java.awt.Color(255, 204, 204));

        jLabel15.setText("Jenis Kamar");

        jLabel16.setText("Tarif");

        tambahKamarButton.setText("Tambah Kamar");
        tambahKamarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahKamarButtonActionPerformed(evt);
            }
        });

        tabelKamar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No. Kamar", "Tipe Kamar", "Jenis Kamar", "Tarif"
            }
        ));
        tabelKamar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelKamarKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelKamar);

        prosesButton.setText("Proses");
        prosesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prosesButtonActionPerformed(evt);
            }
        });

        previewButton.setText("Laporan");
        previewButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previewButtonActionPerformed(evt);
            }
        });

        try {
            noKamarField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        noKamarField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noKamarFieldActionPerformed(evt);
            }
        });

        jLabel11.setText("Biaya Tambahan");

        jLabel12.setText("Total Tagihan");

        jLabel19.setText("Sub Total");

        tambahanField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        tambahanField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tambahanField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tambahanFieldActionPerformed(evt);
            }
        });
        tambahanField.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tambahanFieldPropertyChange(evt);
            }
        });

        subTotalField.setEditable(false);
        subTotalField.setBackground(new java.awt.Color(255, 204, 204));
        subTotalField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        subTotalField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        subTotalField.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                subTotalFieldPropertyChange(evt);
            }
        });

        totalField.setEditable(false);
        totalField.setBackground(new java.awt.Color(255, 204, 204));
        totalField.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        tarifField.setBackground(new java.awt.Color(255, 204, 204));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 78, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(prosesButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(previewButton))
                            .addComponent(tambahKamarButton, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tambahanField)
                                    .addComponent(subTotalField)
                                    .addComponent(totalField, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel4Layout.createSequentialGroup()
                                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jenisKamarField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel4Layout.createSequentialGroup()
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(tarifField, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel4Layout.createSequentialGroup()
                                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(tipeKamarField))
                                .addGroup(jPanel4Layout.createSequentialGroup()
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(noKamarField, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(noKamarField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(tipeKamarField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jenisKamarField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(tarifField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tambahKamarButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(subTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(tambahanField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(totalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(prosesButton)
                    .addComponent(previewButton)))
        );

        javax.swing.GroupLayout transPaneLayout = new javax.swing.GroupLayout(transPane);
        transPane.setLayout(transPaneLayout);
        transPaneLayout.setHorizontalGroup(
            transPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        transPaneLayout.setVerticalGroup(
            transPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainPane.addTab("Transaksi", transPane);

        javax.swing.GroupLayout previewPaneLayout = new javax.swing.GroupLayout(previewPane);
        previewPane.setLayout(previewPaneLayout);
        previewPaneLayout.setHorizontalGroup(
            previewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 819, Short.MAX_VALUE)
        );
        previewPaneLayout.setVerticalGroup(
            previewPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 439, Short.MAX_VALUE)
        );

        mainPane.addTab("Preview", previewPane);

        jLabel18.setText("HOTEL NYAMAN SEKALI");

        keluarButton.setText("Keluar");
        keluarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keluarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPane, javax.swing.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(keluarButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(keluarButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainPane))
        );

        pack();
    }// </editor-fold>                        

    
// <editor-fold defaultstate="collapsed" desc="Event">
    private void noKamarFieldActionPerformed(java.awt.event.ActionEvent evt) {                                             
        if (!JSType.isNumber(noKamarField.getText())
                || noKamarField.getText().length() != 3) {
            setKamar(noKamarField.getText());
            tambahKamarButton.requestFocus();
        } else {
            alertWarning("Nomor kamar salah! Silahkan periksa!");
        }

    }                                            

    private void tambahKamarButtonActionPerformed(java.awt.event.ActionEvent evt) {                                                  

//        if (!isFormValid()) {
//            return;
//        }
        if (tipeKamarField.getText().length() == 0) {
            alertWarning("Silahkan isi data kamar!");
            return;
        }
        tambahKamar();
    }                                                 

    private void tabelKamarKeyPressed(java.awt.event.KeyEvent evt) {                                      
        if (tabelKamar.getSelectedRow() != -1
                && evt.getKeyChar() == evt.VK_DELETE) {
            String index = tabelKamar.getModel().getValueAt(
                    tabelKamar.getSelectedRow(), 0).toString();
            hapusKamar(Integer.valueOf(index));
        }
    }                                     

    private void prosesButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        simpanTransaksi();
    }                                            

    private void tambahanFieldActionPerformed(java.awt.event.ActionEvent evt) {                                              
        setTotalTagihan();
    }                                             

    private void inFieldPropertyChange(java.beans.PropertyChangeEvent evt) {                                       
        try {
//            alertInfo(inField.getText().length() + "");
            df.parse(inField.getText());
            setSubTotal();
        } catch (Exception e) {

        }
    }                                      

    private void outFieldPropertyChange(java.beans.PropertyChangeEvent evt) {                                        
        try {
//            if(inField.getText().length() != 8) return;
            df.parse(outField.getText());
            setSubTotal();
        } catch (Exception e) {

        }
    }                                       

    private void depositFieldPropertyChange(java.beans.PropertyChangeEvent evt) {                                            
        if (depositField.getText().trim().length() != 0) {
            setTotalTagihan();
        }
    }                                           

    private void tambahanFieldPropertyChange(java.beans.PropertyChangeEvent evt) {                                             
        if (tambahanField.getText().trim().length() != 0) {
            setTotalTagihan();
        }
    }                                            

    private void subTotalFieldPropertyChange(java.beans.PropertyChangeEvent evt) {                                             
        if (subTotalField.getText().trim().length() != 0) {
            setTotalTagihan();
        }
    }                                            

    private void keluarButtonActionPerformed(java.awt.event.ActionEvent evt) {                                             
        if(confirm("Apakah anda yakin akan keluar?")){
            System.exit(0);
        }
    }                                            

    private void previewButtonActionPerformed(java.awt.event.ActionEvent evt) {                                              
        showDialog();
    }                                             

    private void depositFieldKeyPressed(java.awt.event.KeyEvent evt) {                                        
        
    }                                       
// </editor-fold>
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Windows".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }
    
// <editor-fold defaultstate="collapsed" desc="Atribut">
    // Variables declaration - do not modify                     
    private javax.swing.JFormattedTextField depositField;
    private javax.swing.JTextField hpField;
    private javax.swing.JFormattedTextField inField;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jenisKamarField;
    private javax.swing.JButton keluarButton;
    private javax.swing.JTextField ktpField;
    private javax.swing.JTabbedPane mainPane;
    private javax.swing.JFormattedTextField noKamarField;
    private javax.swing.JTextField noTransField;
    private javax.swing.JTextArea notaArea;
    private javax.swing.JFormattedTextField outField;
    private javax.swing.JTextField pelangganField;
    private javax.swing.JButton previewButton;
    private javax.swing.JPanel previewPane;
    private javax.swing.JButton prosesButton;
    private javax.swing.JDialog reportDialog;
    private javax.swing.JFormattedTextField subTotalField;
    private javax.swing.JTable tabelKamar;
    private javax.swing.JButton tambahKamarButton;
    private javax.swing.JFormattedTextField tambahanField;
    private javax.swing.JFormattedTextField tarifField;
    private javax.swing.JTextField tglTransField;
    private javax.swing.JTextField tipeKamarField;
    private javax.swing.JFormattedTextField totalField;
    private javax.swing.JPanel transPane;
    // End of variables declaration                   
// </editor-fold>

}
